//
//  main.m
//  TouchBarPlaying
//
//  Created by Chris Jones on 24/11/2016.
//  Copyright © 2016 Chris Jones. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
