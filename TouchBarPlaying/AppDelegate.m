//
//  AppDelegate.m
//  TouchBarPlaying
//
//  Created by Chris Jones on 24/11/2016.
//  Copyright © 2016 Chris Jones. All rights reserved.
//

#import "AppDelegate.h"
#import "NSApplicationWithTouchBar.h"

#pragma mark - Apple Private API
//FIXME: Not sure this is quite the right function signature
void DFRElementSetControlStripPresenceForIdentifier(NSString *identifier, BOOL display);
void DFRSystemModalShowsCloseBoxWhenFrontMost(BOOL flag);

@protocol NSFunctionBarSystemTray
+ (void)addSystemTrayItem:(NSTouchBarItem *)item;
+ (void)removeSystemTrayItem:(NSTouchBarItem *)item;
@end
@interface NSTouchBarItem (NSFunctionBarSystemTray)
+ (void)addSystemTrayItem:(NSTouchBarItem *)item;
+ (void)removeSystemTrayItem:(NSTouchBarItem *)item;
@end

@protocol NSTouchBarSystemTray
+ (void)presentSystemModalFunctionBar:(NSTouchBar *)touchBar systemTrayItemIdentifier:(NSString *)identifier;
+ (void)minimizeSystemModalFunctionBar:(NSTouchBar *)touchBar;
+ (void)dismissSystemModalFunctionBar:(NSTouchBar *)touchBar;
@end
@interface NSTouchBar (NSTouchBarSystemTray)
+ (void)presentSystemModalFunctionBar:(NSTouchBar *)touchBar systemTrayItemIdentifier:(NSString *)identifier;
+ (void)minimizeSystemModalFunctionBar:(NSTouchBar *)touchBar;
+ (void)dismissSystemModalFunctionBar:(NSTouchBar *)touchBar;
@end

#pragma mark - AppDelegate

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    NSLog(@"Starting...");

    self.imageView = [self buildTouchBarButtonWithImage:[NSApp applicationIconImage]
                                                 target:self
                                                 action:@selector(systemButtonAction:)
                                             isTemplate:NO];

    self.textView = [self buildTouchBarButtonWithText:@"Hi!"
                                               target:self
                                               action:@selector(systemButtonAction:)];

    self.systemItem = [self buildTouchBarItemWithView:self.imageView
                                           identifier:@"net.tenshu.TouchBar.systemItem"];

    self.systemItem.view = self.imageView;

    [self addControlStripItem:self.systemItem];
    [self buildSystemTouchBar];
    // This works without having to come from the TouchBar itself: [self systemButtonAction:nil];

    NSLog(@"All done!");
}

- (NSCustomTouchBarItem *)buildTouchBarItemWithView:(NSView *)view identifier:(NSString *)identifier {
    NSCustomTouchBarItem *item = [[NSCustomTouchBarItem alloc] initWithIdentifier:identifier];
    item.view = view;
    item.view.appearance = [NSAppearance appearanceNamed:@"NSAppearanceNameControlStrip"];

    NSLog(@"Built touchbar item %@ with view %@ and appearance %@", item, item.view, item.view.appearance);

    return item;
}

- (NSView *)buildTouchBarButtonWithImage:(NSImage *)image target:(id)target action:(SEL)action isTemplate:(BOOL)template {
    NSButton *button = [NSButton buttonWithImage:image target:target action:action];
    button.image.template = template;

    NSLog(@"Built touchbar button %@ with image %@ and template %@", button, button.image, button.image.template ? @"YES" : @"NO");

    return (NSView *)button;
}

- (NSView *)buildTouchBarButtonWithText:(NSString *)text target:(id)target action:(SEL)action {
    NSButton *button = [NSButton buttonWithTitle:text target:target action:action];

    NSLog(@"Built touchbar button %@ with text %@", button, button.title);

    return (NSView *)button;
}

- (void)addControlStripItem:(NSCustomTouchBarItem *)item {
    // You might consider wrapping this in a check for [NSTouchBarItem respondsToSelector:@(addSystemTrayItem:)] for future safety
    [NSTouchBarItem addSystemTrayItem:item];
    DFRElementSetControlStripPresenceForIdentifier(item.identifier, YES);
    NSLog(@"Added Control Strip item: %@", item);
}

- (void)removeControlStripItem:(NSCustomTouchBarItem *)item {
    DFRElementSetControlStripPresenceForIdentifier(item.identifier, NO);
    [NSTouchBarItem removeSystemTrayItem:item];
    NSLog(@"Removed Control Strip item: %@", item);
}

- (void)buildSystemTouchBar {
    NSTouchBar *bar = [[NSTouchBar alloc] init];
    self.systemBar = bar;
    bar.delegate = self;
    [bar setDefaultItemIdentifiers:@[@"net.tenshu.TouchBar.systemItems.1"]];
}

- (NSTouchBar *)makeTouchBar {
    return self.systemBar;
}

- (NSTouchBarItem *)touchBar:(NSTouchBar *)touchBar makeItemForIdentifier:(NSTouchBarItemIdentifier)identifier {
    NSCustomTouchBarItem *item = [[NSCustomTouchBarItem alloc] initWithIdentifier:@"net.tenshu.TouchBar.systemItems.1"];
    NSButton *button = [NSButton buttonWithTitle:@"FIRST" target:self action:@selector(firstButtonAction:)];
    item.view = button;
    return item;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (IBAction)systemButtonAction:(id)sender
{
    NSLog(@"%@ was pressed", ((NSButton *)sender).title);

    // FIXME: For some reason, when we get here, our button disappears

    DFRSystemModalShowsCloseBoxWhenFrontMost(YES); // This doesn't seem to be necessary in the other apps
    [NSTouchBar presentSystemModalFunctionBar:self.systemBar systemTrayItemIdentifier:self.systemItem.identifier];
}

- (IBAction)firstButtonAction:(id)sender
{
    NSLog(@"%@ was pressed", ((NSButton *)sender).title);
    [NSTouchBar minimizeSystemModalFunctionBar:self.systemBar];
}

@end
