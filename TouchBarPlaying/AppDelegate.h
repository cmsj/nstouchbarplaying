//
//  AppDelegate.h
//  TouchBarPlaying
//
//  Created by Chris Jones on 24/11/2016.
//  Copyright © 2016 Chris Jones. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, NSTouchBarDelegate>

@property (nonatomic, strong) NSCustomTouchBarItem *systemItem;
@property (nonatomic, strong) NSTouchBar *systemBar;
@property (nonatomic, strong) NSView *imageView;
@property (nonatomic, strong) NSView *textView;

- (NSCustomTouchBarItem *)buildTouchBarItemWithView:(NSView *)view identifier:(NSString *)identifier;
- (NSView *)buildTouchBarButtonWithImage:(NSImage *)image target:(id)target action:(SEL)action isTemplate:(BOOL)template;
- (NSView *)buildTouchBarButtonWithText:(NSString *)text target:(id)target action:(SEL)action;
- (void)addControlStripItem:(NSCustomTouchBarItem *)item;
- (void)removeControlStripItem:(NSCustomTouchBarItem *)item;
@end

