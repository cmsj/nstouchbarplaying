//
//  NSApplicationWithTouchBar.h
//  TouchBarPlaying
//
//  Created by Chris Jones on 25/11/2016.
//  Copyright © 2016 Chris Jones. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSApplicationWithTouchBar : NSApplication <NSTouchBarDelegate>

@property (strong, nonatomic) NSTouchBar *currentTouchbar;

@end
