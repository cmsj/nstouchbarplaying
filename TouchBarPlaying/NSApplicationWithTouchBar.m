//
//  NSApplicationWithTouchBar.m
//  TouchBarPlaying
//
//  Created by Chris Jones on 25/11/2016.
//  Copyright © 2016 Chris Jones. All rights reserved.
//

#import "NSApplicationWithTouchBar.h"

@implementation NSApplicationWithTouchBar

- (NSTouchBar *)makeTouchBar {
    return self.currentTouchbar;
}

- (void)removeCurrentTouchBar {
    self.touchBar = nil;
}

@end
